package cs.mad.flashcards.activities

import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.room.Room
import cs.mad.flashcards.adapters.FlashcardAdapter
import cs.mad.flashcards.databinding.ActivityFlashcardSetDetailBinding
import cs.mad.flashcards.entities.FlashCardDao
import cs.mad.flashcards.entities.Database
import cs.mad.flashcards.entities.Flashcard
import cs.mad.flashcards.runOnIO
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

class FlashcardSetDetailActivity : AppCompatActivity() {
    private lateinit var binding: ActivityFlashcardSetDetailBinding
    private lateinit var flashDao: FlashCardDao
    private lateinit var prefs: SharedPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityFlashcardSetDetailBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)



        val db = Room.databaseBuilder(
            applicationContext,
            Database::class.java,Database.databaseName
        ).build()
        flashDao = db.flashDao()
//        prefs = getSharedPreferences("flashCards", MODE_PRIVATE)




        //make this not hard coded
        binding.flashcardList.adapter = FlashcardAdapter(listOf(), flashDao)
        loadData()





        binding.addFlashcardButton.setOnClickListener {

            /////////////////////////////////
            // how do i make id auto generate
            var newCard = Flashcard(null,"test","test")
            runOnIO {  val id = flashDao.insert(newCard)
                    newCard.id = id
            }
            /////////////////////////////////
            (binding.flashcardList.adapter as FlashcardAdapter).addItem(newCard)
                binding.flashcardList.smoothScrollToPosition((binding.flashcardList.adapter as FlashcardAdapter).itemCount - 1)
        }

        binding.studySetButton.setOnClickListener {
            startActivity(Intent(this, StudySetActivity::class.java))
        }


        binding.deleteSetButton.setOnClickListener{
            onBackPressed()
        }

    }

    private fun loadData(){
        runOnIO { (binding.flashcardList.adapter as FlashcardAdapter).update(flashDao.getAll())}
    }




}