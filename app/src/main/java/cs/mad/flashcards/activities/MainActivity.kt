package cs.mad.flashcards.activities

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.room.Room
import cs.mad.flashcards.adapters.FlashcardAdapter
import cs.mad.flashcards.adapters.FlashcardSetAdapter
import cs.mad.flashcards.databinding.ActivityMainBinding
import cs.mad.flashcards.entities.Database
import cs.mad.flashcards.entities.FlashCardSetDao
import cs.mad.flashcards.entities.Flashcard
import cs.mad.flashcards.entities.FlashcardSet
import cs.mad.flashcards.runOnIO

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private lateinit var flashcardsetDao: FlashCardSetDao

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val db = Room.databaseBuilder(
            applicationContext,
            Database::class.java, Database.databaseName
        ).build()

        flashcardsetDao = db.flashSetDao()

        binding.flashcardSetList.adapter = FlashcardSetAdapter(listOf())
        loadData()




        // make this not hardcoded but fetch from database
//        binding.flashcardSetList.adapter = FlashcardSetAdapter(FlashcardSet.getHardcodedFlashcardSets())

        binding.createSetButton.setOnClickListener {
            var newSet = FlashcardSet(null,"test Set")
            runOnIO {  val id = flashcardsetDao.insert(newSet)
                newSet.id_num = id
            }

            (binding.flashcardSetList.adapter as FlashcardSetAdapter).addItem(newSet)
            binding.flashcardSetList.smoothScrollToPosition((binding.flashcardSetList.adapter as FlashcardSetAdapter).itemCount - 1)
        }
    }


    private fun loadData(){
        runOnIO { (binding.flashcardSetList.adapter as FlashcardSetAdapter).update(flashcardsetDao.getAll())}
    }
}