package cs.mad.flashcards.entities

import androidx.room.*

@Entity
data class Flashcard(
    // how to make auto generate
    @PrimaryKey(autoGenerate = true) var id: Long?,
    var question: String,
    var answer: String
) {
    companion object {
        fun getHardcodedFlashcards(): List<Flashcard> {
            val cards = mutableListOf<Flashcard>()
            for (i in 1..10) {
                cards.add(Flashcard(null,"Term $i", "Def $i"))
            }
            return cards
        }
    }
}

@Dao
interface FlashCardDao{
    @Query("select * from Flashcard")
    suspend fun getAll(): List<Flashcard>

    @Insert
    suspend fun insert(flashcard: Flashcard): Long

    @Insert
    suspend fun insertAll(flashcards: List<Flashcard>)

    @Update
    suspend fun update(flashcard: Flashcard)

    @Delete
    suspend fun delete(flashcard: Flashcard)



}