package cs.mad.flashcards.entities

import androidx.room.Database
import androidx.room.RoomDatabase


// database for flashcard and flashcardsets?

@Database (entities = [Flashcard::class, FlashcardSet::class], version = 1)
abstract class Database: RoomDatabase(){
    companion object{
        const val databaseName = "FlashCard_Database"
    }



    abstract fun flashDao(): FlashCardDao
    abstract fun flashSetDao(): FlashCardSetDao

}