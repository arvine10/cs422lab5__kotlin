package cs.mad.flashcards.entities

import androidx.room.*


@Entity
data class FlashcardSet(
    @PrimaryKey(autoGenerate = true) var id_num: Long?,
    val title: String, val id: Long? = null) {
    companion object {
        fun getHardcodedFlashcardSets(): List<FlashcardSet> {
            val sets = mutableListOf<FlashcardSet>()
            for (i in 1..10) {
                sets.add(FlashcardSet(null,"Set $i"))
            }
            return sets
        }
    }
}


@Dao
interface FlashCardSetDao{
    @Query("select * from Flashcardset")
    suspend fun getAll(): List<FlashcardSet>

    @Insert
    suspend fun insert(flashcardset: FlashcardSet): Long

    @Insert
    suspend fun insertAll(flashcardsets: List<FlashcardSet>)

    @Update
    suspend fun update(flashcardset: FlashcardSet)

    @Delete
    suspend fun delete(flashcardset: FlashcardSet)



}