package cs.mad.flashcards.entities;

import androidx.annotation.NonNull;
import androidx.room.DatabaseConfiguration;
import androidx.room.InvalidationTracker;
import androidx.room.RoomOpenHelper;
import androidx.room.RoomOpenHelper.Delegate;
import androidx.room.RoomOpenHelper.ValidationResult;
import androidx.room.migration.AutoMigrationSpec;
import androidx.room.migration.Migration;
import androidx.room.util.DBUtil;
import androidx.room.util.TableInfo;
import androidx.room.util.TableInfo.Column;
import androidx.room.util.TableInfo.ForeignKey;
import androidx.room.util.TableInfo.Index;
import androidx.sqlite.db.SupportSQLiteDatabase;
import androidx.sqlite.db.SupportSQLiteOpenHelper;
import androidx.sqlite.db.SupportSQLiteOpenHelper.Callback;
import androidx.sqlite.db.SupportSQLiteOpenHelper.Configuration;
import java.lang.Class;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

@SuppressWarnings({"unchecked", "deprecation"})
public final class Database_Impl extends Database {
  private volatile FlashCardDao _flashCardDao;

  private volatile FlashCardSetDao _flashCardSetDao;

  @Override
  protected SupportSQLiteOpenHelper createOpenHelper(DatabaseConfiguration configuration) {
    final SupportSQLiteOpenHelper.Callback _openCallback = new RoomOpenHelper(configuration, new RoomOpenHelper.Delegate(1) {
      @Override
      public void createAllTables(SupportSQLiteDatabase _db) {
        _db.execSQL("CREATE TABLE IF NOT EXISTS `Flashcard` (`id` INTEGER PRIMARY KEY AUTOINCREMENT, `question` TEXT NOT NULL, `answer` TEXT NOT NULL)");
        _db.execSQL("CREATE TABLE IF NOT EXISTS `FlashcardSet` (`id_num` INTEGER PRIMARY KEY AUTOINCREMENT, `title` TEXT NOT NULL, `id` INTEGER)");
        _db.execSQL("CREATE TABLE IF NOT EXISTS room_master_table (id INTEGER PRIMARY KEY,identity_hash TEXT)");
        _db.execSQL("INSERT OR REPLACE INTO room_master_table (id,identity_hash) VALUES(42, 'e5440679ef1387269621cd822adb727f')");
      }

      @Override
      public void dropAllTables(SupportSQLiteDatabase _db) {
        _db.execSQL("DROP TABLE IF EXISTS `Flashcard`");
        _db.execSQL("DROP TABLE IF EXISTS `FlashcardSet`");
        if (mCallbacks != null) {
          for (int _i = 0, _size = mCallbacks.size(); _i < _size; _i++) {
            mCallbacks.get(_i).onDestructiveMigration(_db);
          }
        }
      }

      @Override
      protected void onCreate(SupportSQLiteDatabase _db) {
        if (mCallbacks != null) {
          for (int _i = 0, _size = mCallbacks.size(); _i < _size; _i++) {
            mCallbacks.get(_i).onCreate(_db);
          }
        }
      }

      @Override
      public void onOpen(SupportSQLiteDatabase _db) {
        mDatabase = _db;
        internalInitInvalidationTracker(_db);
        if (mCallbacks != null) {
          for (int _i = 0, _size = mCallbacks.size(); _i < _size; _i++) {
            mCallbacks.get(_i).onOpen(_db);
          }
        }
      }

      @Override
      public void onPreMigrate(SupportSQLiteDatabase _db) {
        DBUtil.dropFtsSyncTriggers(_db);
      }

      @Override
      public void onPostMigrate(SupportSQLiteDatabase _db) {
      }

      @Override
      protected RoomOpenHelper.ValidationResult onValidateSchema(SupportSQLiteDatabase _db) {
        final HashMap<String, TableInfo.Column> _columnsFlashcard = new HashMap<String, TableInfo.Column>(3);
        _columnsFlashcard.put("id", new TableInfo.Column("id", "INTEGER", false, 1, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsFlashcard.put("question", new TableInfo.Column("question", "TEXT", true, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsFlashcard.put("answer", new TableInfo.Column("answer", "TEXT", true, 0, null, TableInfo.CREATED_FROM_ENTITY));
        final HashSet<TableInfo.ForeignKey> _foreignKeysFlashcard = new HashSet<TableInfo.ForeignKey>(0);
        final HashSet<TableInfo.Index> _indicesFlashcard = new HashSet<TableInfo.Index>(0);
        final TableInfo _infoFlashcard = new TableInfo("Flashcard", _columnsFlashcard, _foreignKeysFlashcard, _indicesFlashcard);
        final TableInfo _existingFlashcard = TableInfo.read(_db, "Flashcard");
        if (! _infoFlashcard.equals(_existingFlashcard)) {
          return new RoomOpenHelper.ValidationResult(false, "Flashcard(cs.mad.flashcards.entities.Flashcard).\n"
                  + " Expected:\n" + _infoFlashcard + "\n"
                  + " Found:\n" + _existingFlashcard);
        }
        final HashMap<String, TableInfo.Column> _columnsFlashcardSet = new HashMap<String, TableInfo.Column>(3);
        _columnsFlashcardSet.put("id_num", new TableInfo.Column("id_num", "INTEGER", false, 1, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsFlashcardSet.put("title", new TableInfo.Column("title", "TEXT", true, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsFlashcardSet.put("id", new TableInfo.Column("id", "INTEGER", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        final HashSet<TableInfo.ForeignKey> _foreignKeysFlashcardSet = new HashSet<TableInfo.ForeignKey>(0);
        final HashSet<TableInfo.Index> _indicesFlashcardSet = new HashSet<TableInfo.Index>(0);
        final TableInfo _infoFlashcardSet = new TableInfo("FlashcardSet", _columnsFlashcardSet, _foreignKeysFlashcardSet, _indicesFlashcardSet);
        final TableInfo _existingFlashcardSet = TableInfo.read(_db, "FlashcardSet");
        if (! _infoFlashcardSet.equals(_existingFlashcardSet)) {
          return new RoomOpenHelper.ValidationResult(false, "FlashcardSet(cs.mad.flashcards.entities.FlashcardSet).\n"
                  + " Expected:\n" + _infoFlashcardSet + "\n"
                  + " Found:\n" + _existingFlashcardSet);
        }
        return new RoomOpenHelper.ValidationResult(true, null);
      }
    }, "e5440679ef1387269621cd822adb727f", "c5252927cfc87e005ef945028c421aab");
    final SupportSQLiteOpenHelper.Configuration _sqliteConfig = SupportSQLiteOpenHelper.Configuration.builder(configuration.context)
        .name(configuration.name)
        .callback(_openCallback)
        .build();
    final SupportSQLiteOpenHelper _helper = configuration.sqliteOpenHelperFactory.create(_sqliteConfig);
    return _helper;
  }

  @Override
  protected InvalidationTracker createInvalidationTracker() {
    final HashMap<String, String> _shadowTablesMap = new HashMap<String, String>(0);
    HashMap<String, Set<String>> _viewTables = new HashMap<String, Set<String>>(0);
    return new InvalidationTracker(this, _shadowTablesMap, _viewTables, "Flashcard","FlashcardSet");
  }

  @Override
  public void clearAllTables() {
    super.assertNotMainThread();
    final SupportSQLiteDatabase _db = super.getOpenHelper().getWritableDatabase();
    try {
      super.beginTransaction();
      _db.execSQL("DELETE FROM `Flashcard`");
      _db.execSQL("DELETE FROM `FlashcardSet`");
      super.setTransactionSuccessful();
    } finally {
      super.endTransaction();
      _db.query("PRAGMA wal_checkpoint(FULL)").close();
      if (!_db.inTransaction()) {
        _db.execSQL("VACUUM");
      }
    }
  }

  @Override
  protected Map<Class<?>, List<Class<?>>> getRequiredTypeConverters() {
    final HashMap<Class<?>, List<Class<?>>> _typeConvertersMap = new HashMap<Class<?>, List<Class<?>>>();
    _typeConvertersMap.put(FlashCardDao.class, FlashCardDao_Impl.getRequiredConverters());
    _typeConvertersMap.put(FlashCardSetDao.class, FlashCardSetDao_Impl.getRequiredConverters());
    return _typeConvertersMap;
  }

  @Override
  public Set<Class<? extends AutoMigrationSpec>> getRequiredAutoMigrationSpecs() {
    final HashSet<Class<? extends AutoMigrationSpec>> _autoMigrationSpecsSet = new HashSet<Class<? extends AutoMigrationSpec>>();
    return _autoMigrationSpecsSet;
  }

  @Override
  public List<Migration> getAutoMigrations(
      @NonNull Map<Class<? extends AutoMigrationSpec>, AutoMigrationSpec> autoMigrationSpecsMap) {
    return Arrays.asList();
  }

  @Override
  public FlashCardDao flashDao() {
    if (_flashCardDao != null) {
      return _flashCardDao;
    } else {
      synchronized(this) {
        if(_flashCardDao == null) {
          _flashCardDao = new FlashCardDao_Impl(this);
        }
        return _flashCardDao;
      }
    }
  }

  @Override
  public FlashCardSetDao flashSetDao() {
    if (_flashCardSetDao != null) {
      return _flashCardSetDao;
    } else {
      synchronized(this) {
        if(_flashCardSetDao == null) {
          _flashCardSetDao = new FlashCardSetDao_Impl(this);
        }
        return _flashCardSetDao;
      }
    }
  }
}
